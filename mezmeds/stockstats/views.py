import json

from django.shortcuts import render
from django.http import HttpResponse

from sqldirect.sqldirect import *

#!!! Should move "orders functions to a new app to be more modular ---> Refactor in Future

def index(request):



    return render(request, 'stockstats/index.html')

# GETS CURRENT CLINIC DATA
def get_graph_data(request):

    if request.method == 'POST':

        clinicSelected = request.POST.get('globalClinicSelected')
        year = '2017' # NOTE This needs to be dynamically selected in future
        clinicObj = Statistics()

        clinic = clinicObj.barGraphMeds(clinicSelected,year)
        clinicTras = list(clinic)

        datagraph  = dataprocessor(clinicTras)

        tmp = ("Month","Nevirapine","Stavudine", "Zidotabine")
        datagraph.insert(0,tmp)

        data = json.dumps(datagraph)
        print ("@@@@@@@@@",data)

        return HttpResponse(
                data,
                content_type="application/json"
            )


    else:

            return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json"
        )

#!!! Refactor, very ugly solution but approach is due to data structure of google graphs and database pull/structure--> fix if possible
def dataprocessor(data):

    dates = ["January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"]

    dataList = []
    for i in range(12):

        tmp = [dates[i],0,0,0]
        dataList.append(tmp)

    for i, x in enumerate(data):

        if data[i][0] == '01' and data[i][1]=='nevirapine':
            dataList[0][1] = data[i][2]
        elif data[i][0] == '01' and data[i][1]=='stavudine':
            dataList[0][2] = data[i][2]
        elif data[i][0] == '01' and data[i][1]=='zidotabine':
            dataList[0][2] = data[i][2] #nevirapine


        if data[i][0] == '02' and data[i][1]=='nevirapine':
            dataList[1][1] = data[i][2]
        elif data[i][0] == '02' and data[i][1]=='stavudine':
            dataList[1][2] = data[i][2]
        elif data[i][0] == '02' and data[i][1]=='zidotabine':
            dataList[1][2] = data[i][2] #nevirapine

        if data[i][0] == '03' and data[i][1]=='nevirapine':
            dataList[2][1] = data[i][2]
        elif data[i][0] == '03' and data[i][1]=='stavudine':
            dataList[2][2] = data[i][2]
        elif data[i][0] == '03' and data[i][1]=='zidotabine':
            dataList[2][2] = data[i][2] #nevirapine

        if data[i][0] == '04' and data[i][1]=='nevirapine':
            dataList[3][1] = data[i][2]
        elif data[i][0] == '04' and data[i][1]=='stavudine':
            dataList[3][2] = data[i][2]
        elif data[i][0] == '04' and data[i][1]=='zidotabine':
            dataList[3][2] = data[i][2] #nevirapine

        if data[i][0] == '05' and data[i][1]=='nevirapine':
            dataList[4][1] = data[i][2]
        elif data[i][0] == '05' and data[i][1]=='stavudine':
            dataList[4][2] = data[i][2]
        elif data[i][0] == '05' and data[i][1]=='zidotabine':
            dataList[4][2] = data[i][2] #nevirapine

        if data[i][0] == '06' and data[i][1]=='nevirapine':
            dataList[5][1] = data[i][2]
        elif data[i][0] == '06' and data[i][1]=='stavudine':
            dataList[5][2] = data[i][2]
        elif data[i][0] == '06' and data[i][1]=='zidotabine':
            dataList[5][2] = data[i][2] #nevirapine

        if data[i][0] == '07' and data[i][1]=='nevirapine':
            dataList[6][1] = data[i][2]
        elif data[i][0] == '07' and data[i][1]=='stavudine':
            dataList[6][2] = data[i][2]
        elif data[i][0] == '07' and data[i][1]=='zidotabine':
            dataList[6][2] = data[i][2] #nevirapine

        if data[i][0] == '08' and data[i][1]=='nevirapine':
            dataList[7][1] = data[i][2]
        elif data[i][0] == '08' and data[i][1]=='stavudine':
            dataList[7][2] = data[i][2]
        elif data[i][0] == '08' and data[i][1]=='zidotabine':
            dataList[7][2] = data[i][2] #nevirapine


        if data[i][0] == '09' and data[i][1]=='nevirapine':
            dataList[8][1] = data[i][2]
        elif data[i][0] == '09' and data[i][1]=='stavudine':
            dataList[8][2] = data[i][2]
        elif data[i][0] == '09' and data[i][1]=='zidotabine':
            dataList[8][2] = data[i][2] #nevirapine

        if data[i][0] == '10' and data[i][1]=='nevirapine':
            dataList[9][1] = data[i][2]
        elif data[i][0] == '10' and data[i][1]=='stavudine':
            dataList[9][2] = data[i][2]
        elif data[i][0] == '10' and data[i][1]=='zidotabine':
            dataList[9][2] = data[i][2] #nevirapine

        if data[i][0] == '11' and data[i][1]=='nevirapine':
            dataList[10][1] = data[i][2]
        elif data[i][0] == '11' and data[i][1]=='stavudine':
            dataList[10][2] = data[i][2]
        elif data[i][0] == '11' and data[i][1]=='zidotabine':
            dataList[10][2] = data[i][2] #nevirapine

        if data[i][0] == '12' and data[i][1]=='nevirapine':
            dataList[11][1] = data[i][2]
        elif data[i][0] == '12' and data[i][1]=='stavudine':
            dataList[11][2] = data[i][2]
        elif data[i][0] == '12' and data[i][1]=='zidotabine':
            dataList[11][2] = data[i][2] #nevirapine
    return dataList
