from django.apps import AppConfig


class StockstatsConfig(AppConfig):
    name = 'stockstats'
