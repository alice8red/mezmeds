import sqlite3
import datetime

class Base():

    def __init__(self):

        self.con = sqlite3.connect('db.sqlite3')
        self.cur = self.con.cursor()

# Used for Ajax Dropdowns
# Get Clinic List

#***##############LISTS##################
class ClinicsAll(Base):

    def __init__(self):
        super().__init__()


    def getAll(self):

        self.cur.execute('SELECT name FROM Clinic ')

        output = self.cur.fetchall()

        self.cur.close()

        return output


# Used for Ajax Dropdowns
# Get Medication List
class MedicationAll(Base):

    def __init__(self):
        super().__init__()


    def getAll(self):

        self.cur.execute('SELECT name FROM Medication ')

        output = self.cur.fetchall()

        self.cur.close()

        return output

#***############STOCKLEVELS AND UPDATE####################

# GET  individual Clinic StockLevels
class ClinicIndividualStock(Base):

    def __init__(self,clinic):

        super().__init__()
        self.clinic = clinic

    def getClinicData(self):


        self.cur.execute('''SELECT Stock.NameOfMedication,Stock.Stock, Stock.TotalAmountUsed
                            FROM Stock
                            LEFT JOIN Clinic ON Clinic.ClinicID = Stock.ClinicID
                            WHERE Clinic.Name = (?)''',(self.clinic ,))


        output = self.cur.fetchall()

        self.cur.close()

        return output

# Manage Stock Levels

class StockUpdated(Base):

    def __init__(self,clinic="default", meds ="default", newAmount = 0):
        super().__init__()

        self.clinic = clinic
        self.meds = meds
        self.newAmount = newAmount

#Current levels of Stock Update and Post To Logs
    def postStockCurrent(self):



        self.cur.execute('''SELECT Stock.Stock
                            FROM Stock
                            LEFT JOIN Clinic ON Clinic.ClinicID = Stock.ClinicID
                            WHERE Clinic.Name = (?) AND Stock.NameOfMedication =(?) ''',(self.clinic ,self.meds))

        dateNow = datetime.date.today()

        tmp = self.cur.fetchone()

        currentAmount =tmp[0]

        used  = currentAmount - self.newAmount

        self.cur.execute('''INSERT INTO StockUsageLog (ClinicID,LogDate ,NameOfMedication,AmountUsed)
                            VALUES (
                                     ( SELECT ClinicID FROM Clinic WHERE Name= (?) ),
	                                  ((?)),((?)),((?))

                                      )''',(self.clinic,dateNow,self.meds,used) )


        self.cur.execute('''UPDATE Stock
                            SET TotalAmountUsed = TotalAmountUsed +  (?) , Stock = (?)
                            WHERE Stock.ClinicID in
                                  (SELECT Clinic.ClinicID
                                   FROM Clinic
                                   WHERE Clinic.Name = (?) AND Stock.NameOfMedication = (?))''',(self.newAmount,self.newAmount,self.clinic,self.meds,))

        self.con.commit()
        self.cur.close()


#***##################ORDERS########################

#Stock Orders and Updates

    def orderStock (self ,orderAmount,truthValue):
#!!!Should really set dateNows as parameter past rather than cal in function
        dateNow = datetime.date.today()
        print(self.clinic)
        print(self.meds)

        self.cur.execute('''INSERT INTO StockLog (ClinicID,StockID,NameOfMedication,OrderDate ,OrderAmount,OrderRecieved)
                            VALUES (
                                     ( SELECT ClinicID FROM Clinic WHERE Name= (?) ),

                                     (SELECT Stock.StockID
											FROM Stock
                                            LEFT JOIN Clinic ON Clinic.ClinicID = Stock.ClinicID
                                            WHERE Clinic.Name = (?) AND Stock.NameOfMedication =(?)),
	                                  ((?)),((?)),((?)),((?))

                                      )''',(self.clinic,self.clinic,self.meds,self.meds,dateNow,orderAmount,truthValue) )



        self.con.commit()
        self.cur.close()



    def OrderRecieved(date,truthValue):

        self.cur.execute('''UPDATE StockLog
                            SET OrderRecieved = (?)
                            WHERE StockLog.ClinicID in
                                  (SELECT Clinic.ClinicID
                                   FROM Clinic
                                   WHERE Clinic.Name = (?)
                                         AND StockLog.NameOfMedication = (?)
                                         AND StockLog.OrderDate = (?)

                                         )''',(truthValue,self.clinic,self.meds,self.date))

        self.con.commit()
        self.cur.close()

#WARNS USER IF STOCK LEVEL BELOW UNIT.*
    def getAllRestock(self):

        self.cur.execute('''SELECT Clinic.Name, Stock.NameOfMedication, Stock.Stock,
		                      COALESCE(StockLog.OrderAmount,0) as OrderAmount
	                             FROM Stock
                                            LEFT JOIN  Clinic
	                                                       ON Stock.ClinicID = Clinic.ClinicID
	                                        LEFT JOIN StockLog
	                                                       ON StockLog.StockID = Stock.StockID

	                                        WHERE Stock.Stock <= 5''')

        output = self.cur.fetchall()

        self.cur.close()

        return output


#########################DILVERIES#################################

    def getAllDiliviers(self):

        self.cur.execute('''SELECT Clinic.Name,StockLog.Timestamp,StockLog.NameOfMedication, StockLog.OrderAmount,StockLog.LogID

                                    FROM StockLog
                                    LEFT JOIN Clinic
    	                                   ON StockLog.ClinicID = Clinic.ClinicID
    		                            WHERE StockLog.OrderRecieved = 0
                                        ORDER BY datetime(Timestamp) ASC

                                        ''')

        output = self.cur.fetchall()

        self.cur.close()

        return output



    def postDiliviersRecieved(self,LogID):

        self.cur.execute('''UPDATE StockLog
                            SET OrderRecieved = 1
                            WHERE StockLog.LogID = (?)

                                        ''',(LogID,))

        self.cur.execute('''SELECT StockLog.OrderAmount,StockLog.StockID

                                    FROM StockLog
                                         WHERE StockLog.LogID = (?)


                                        ''',(LogID,))

        output = self.cur.fetchone()

        self.cur.execute('''UPDATE Stock
                            SET Stock = (?)
                            WHERE Stock.StockID = (?)

                                        ''',output)

        self.con.commit()

        self.cur.close()


# STATS
class Statistics(Base):

        def __init__(self,clinic="default", meds ="default", newAmount = 0):
            super().__init__()

            self.clinic = clinic
            self.meds = meds


        def barGraphMeds(self,clinic,year):


            self.cur.execute('''SELECT strftime( '%m', StockUsageLog.LogDate) ,StockUsageLog.NameOfMedication ,SUM(StockUsageLog.AmountUsed)
                                        FROM   StockUsageLog
                                            LEFT JOIN Clinic
							                                     ON StockUsageLog.ClinicID = Clinic.ClinicID

            		                    WHERE Clinic.Name = (?) AND strftime( '%Y', StockUsageLog.LogDate) = (?)

                                        GROUP  BY StockUsageLog.NameOfMedication,StockUsageLog.LogDate

                                            ''',(clinic,year))

            output = self.cur.fetchall()


            self.cur.close()

            return output
