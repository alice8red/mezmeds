#!/usr/bin/env

import sqlite3
import datetime
import random
import calendar

con = sqlite3.connect('db.sqlite3')
cur = con.cursor()
cur.execute("PRAGMA foreign_keys")

cur.execute('''CREATE TABLE StockUsageLog
                    (LogID    INTEGER  PRIMARY KEY AUTOINCREMENT,
                     ClinicID   INT,
                     LogDate DATE,
		     NameOfMedication TEXT,
                     AmountUsed ,

                    FOREIGN KEY(ClinicID) REFERENCES Clinic(ClinicID))''')



con.commit()
con.close()
