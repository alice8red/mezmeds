#!/usr/bin/env

import sqlite3
import datetime
import random
import calendar

con = sqlite3.connect('db.sqlite3')
cur = con.cursor()
cur.execute("PRAGMA foreign_keys")


meds = (
    (1, 'nevirapine'),
    (2, 'stavudine'),
    (3, 'zidotabine'),

)

clinic = (
    (1, 'Alice'),
    (2, 'Red'),
    (3, 'Rabbit'),
    (4, 'Hatter'),
    (5, 'Twins'),
    (6, 'Hearts'),
    (7, 'Cheshire'),
    (8, 'Caterpillar'),
    (9, 'Hare'),
    (10,'Frontier'),
)

#Create Mock StockList for DB
stockList = []

for i in range(10):

	tmp = []

	for y in range(3):

		tmp = []

		clinicID = tmp.append(clinic[i][0])

		nameOfMedication = tmp.append(meds[y][1])

		stock = tmp.append(10)

		totalAmountUse = tmp.append(4)

		stockList.append(tmp)


stockTuple = tuple(tuple(x) for x in stockList)


#Create Mock Logs as related to current Mock StockList above

StockLog = []
firstJan = datetime.date(2016,1,1)



try:
# Did not use FK for Meds in Stock as deletion of medication (VIA CASCADING)
# would result in lost data of current supply of meds in clinic
    cur.execute('''CREATE TABLE Stock
                    (StockID    INTEGER  PRIMARY KEY AUTOINCREMENT,
                     ClinicID   INT,
                     NameOfMedication TEXT,
                     Stock INT,
                     TotalAmountUsed INT,

                     FOREIGN KEY(ClinicID) REFERENCES Clinic(ClinicID))''')

    queryStock = "INSERT INTO Stock(ClinicID,NameOfMedication,Stock,TotalAmountUsed) VALUES( ?, ?, ?,?)"
    cur.executemany(queryStock,stockTuple)

    con.commit()
# DATA STATISTICS TABLES

    cur.execute('''SELECT  StockID
                        FROM Stock
                          ''')

    output = cur.fetchall()

    print(output)

    for i in range(30):

        tmp = []

        randomDay = firstJan + datetime.timedelta(days = random.randint(0, 365 if calendar.isleap(firstJan.year) else 364))

        clinicID = tmp.append(stockList[i][0])

        StockID = tmp.append(output[i][0])

        OrderDate = tmp.append(randomDay)

        nameOfMedication = tmp.append(stockList[i][1])

        order = tmp.append(4+i)

        recieved = tmp.append(0)

        StockLog.append(tmp)


        stockLogTuple = tuple(tuple(x) for x in StockLog)




    cur.execute('''CREATE TABLE StockLog
                    (LogID    INTEGER  PRIMARY KEY AUTOINCREMENT,
		     ClinicID   INT,
		     StockID INT,
                     OrderDate DATE,
		     Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
		     NameOfMedication TEXT,
                     OrderAmount INT,
		     OrderRecieved INT,

                    FOREIGN KEY(ClinicID) REFERENCES Clinic(ClinicID),
                    FOREIGN KEY(StockID) REFERENCES Stock(StockID)

)''')

    queryStockLog = "INSERT INTO StockLog(ClinicID,StockID,OrderDate,NameOfMedication,OrderAmount,OrderRecieved) VALUES(?, ?, ?, ?,?,?)"
    cur.executemany(queryStockLog,stockLogTuple)

    cur.execute('''CREATE TABLE StockUsageLog
                    (LogID    INTEGER  PRIMARY KEY AUTOINCREMENT,
                     ClinicID   INT,
                     LogDate DATE,
		     Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
		     NameOfMedication TEXT,
                     AmountUsed ,

                    FOREIGN KEY(ClinicID) REFERENCES Clinic(ClinicID))''')

    con.commit()

except Exception as e:

    con.rollback()
    print ('Error %s' % e)
    # raise # coding=utf-8

finally:

    con.close()
