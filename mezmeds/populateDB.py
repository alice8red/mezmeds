#!/usr/bin/env

import sqlite3
import datetime
import random
import calendar

con = sqlite3.connect('db.sqlite3')
cur = con.cursor()
cur.execute("PRAGMA foreign_keys")

cur.execute("PRAGMA foreign_keys")

meds = (
    (1, 'nevirapine'),
    (2, 'stavudine'),
    (3, 'zidotabine'),

)

clinic = (
    (1, 'Alice'),
    (2, 'Red'),
    (3, 'Rabbit'),
    (4, 'Hatter'),
    (5, 'Twins'),
    (6, 'Hearts'),
    (7, 'Cheshire'),
    (8, 'Caterpillar'),
    (9, 'Hare'),
    (10,'Frontier'),
)


try:


# STOCK CONTROL

    cur.execute('''CREATE TABLE Medication
                    (MedicationID   INT PRIMARY KEY,
                     Name           TEXT  )''')

    cur.execute('''CREATE TABLE Clinic
                    (ClinicID   INT PRIMARY KEY,
                     Name       TEXT  )''')

    


    queryMeds = "INSERT INTO Medication(MedicationID,Name) VALUES (?, ?)"

    cur.executemany(queryMeds,meds)

    queryClinics = "INSERT INTO Clinic(ClinicID,Name) VALUES (?,?)"

    cur.executemany(queryClinics,clinic)


   
 
#creates StockUsageLog only   
    cur.execute('''CREATE TABLE StockUsageLog
                    (LogID    INTEGER  PRIMARY KEY AUTOINCREMENT,
                     ClinicID   INT,
                     LogDate DATE,
		     NameOfMedication TEXT,
                     AmountUsed ,

                    FOREIGN KEY(ClinicID) REFERENCES Clinic(ClinicID))''')

	

# DATA STATISTICS TABLES AND ORDERS


    con.commit()

except Exception as e:

    con.rollback()
    print ('Error %s' % e)
    # raise # coding=utf-8

finally:

    con.close()






