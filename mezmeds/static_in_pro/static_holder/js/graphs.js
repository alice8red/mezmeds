// This Script handles Downdown List And Graphs

$(function() {

load_clinic_list()



//GET CLINICS
function load_clinic_list() {


  $.ajax({
      url : "/dropdowns/cliniclist",
      type : "GET",

      success: function(data){
// CLINIC LIST
        var toAppendY = '';
        toAppendY += '<option value="Clinic">' + "Select From List"+ '</option>';
        for(i=0; i<data[0].length; i++) {

        toAppendY += '<option value="' + data[0][i][0]+ '">' + data[0][i][0] + '</option>';

      };

      $('#clinicListY').append(toAppendY);


      $('.chosen').chosen({
        width: '50%',
        allow_single_deselect: true
        });

        $("#clinicListY").on('change',function(){

         globalClinicSelected = $(this).val();

         clinic_bar_graph(globalClinicSelected)

       });

        },

      error : function(xhr,errmsg,err) {


      alert("An internal Error Occured :" + errmsg + " Please Refresh the Page and Try again: If it still does not work then there is a really big issue!!!")

          }
});
};


// AJAX for posting
function clinic_bar_graph(globalClinicSelected) {

        $.ajax({
            url : "/stockstats/bargraph/",
            type : "POST",
            data : {
                    globalClinicSelected : globalClinicSelected,


                },

          success : function(json) {

            $( ".clinicDataRemove" ).empty();

            $( ".clinicDataRemove" ).remove();

            $( "#clinicNameX" ).remove();

            // $("#clinicNameY").prepend( '<h2 id = "clinicNameX"' + 'class ="bg-info text-align-center">'
            //                      +  "Stats For Clinic: " + globalClinicSelected
            //                      +  "</h2>")


            $("#clinicData").prepend( "<div class ='clinicDataRemove' id='columnchart_material' style='width: 900px; height: 500px;'>"
                            +

                            +"</div>")


                            google.charts.load('current', {'packages':['bar']});
                             google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                              var data = google.visualization.arrayToDataTable(json);

                              var options = {
                                chart: {
                                  title: globalClinicSelected,
                                  subtitle: 'Medication Usage By Month',
                                }
                              };

                              var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                              chart.draw(data, options);
                            }








                              },






          error : function(xhr,errmsg,err) {


          alert("An internal Error Occured :" + errmsg + " Please Refresh the Page and Try again: If it still does not work then there is a really big issue!!!")

                }
});
};

// NOTE the CSRF Handling code below was taken from Django Docs directly.

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

});
