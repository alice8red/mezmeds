// This Script handles Downdown List And Post Requests Related to Them

$(function() {

load_clinics()



//GET CLINICS
function load_clinics() {


  $.ajax({
      url : "/dropdowns/cliniclist",
      type : "GET",

      success: function(data){
// CLINIC LIST
        var toAppendY = '';
        for(i=0; i<data[0].length; i++) {
        toAppendY += '<option value="' + data[0][i][0]+ '">' + data[0][i][0] + '</option>';

      };

// Medication LIST
      var toAppendX = '';
      for(i=0; i<data[1].length; i++) {
        toAppendX += '<option value="' + data[1][i][0]+ '">' + data[1][i][0] + '</option>';
        };


      $('#clinicListY').append(toAppendY);
      $('#medsListX').append(toAppendX);
      globalClinicSelected = "Null"

      $('.chosen').chosen({
        width: '50%',
        allow_single_deselect: true
        });

        $("#clinicListY").on('change',function(){

         globalClinicSelected = $(this).val();

         get_post_clinic_data(globalClinicSelected)

       });

       $('#orderMeds').on('submit', function(event){

        event.preventDefault();

       globalClinicSelected = $('#clinicListY').val();
       globalMedsSelected = $('#medsListX').val();
       globalStockAmt = $('#stockvalue').val();
       $('#orderMeds')[0].reset();


        post_stock_data(globalClinicSelected,globalMedsSelected,globalStockAmt)

      });

        },

      error : function(xhr,errmsg,err) {


      alert("An internal Error Occured :" + errmsg + " Please Refresh the Page and Try again: If it still does not work then there is a really big issue!!!")

          }
});
};


// AJAX for posting
function get_post_clinic_data(globalClinicSelected) {

        $.ajax({
            url : "/stockman/clinicdata/",
            type : "POST",
            data : {
                    globalClinicSelected : globalClinicSelected,


                },

          success : function(data) {


            $( ".clinicDataRemove" ).empty();

            $( ".clinicDataRemove" ).remove();

            $( "#clinicNameX" ).remove();

            $("#clinicNameY").prepend( '<h2 id = "clinicNameX"' + 'class ="bg-info text-align-center">'
                                 +  "Stock For Clinic: " + globalClinicSelected
                                 +  "</h2>")

            for (var i = 0; i < data.length; i++) {


      $("#clinicData").prepend( "<tr class ='clinicDataRemove'>"
                           + "<th> " + data[i][0] + "</th>"
                           + "<th> " + data[i][1] + "</th>"
                           + "<th> " + data[i][2] + "</th>"

                            +"</tr>")
                  }
                              },

          error : function(xhr,errmsg,err) {


          alert("An internal Error Occured :" + errmsg + " Please Refresh the Page and Try again: If it still does not work then there is a really big issue!!!")

                }
});
};

// AJAX for posting
function post_stock_data(globalClinicSelected,globalMedsSelected,globalStockAmt) {

        $.ajax({
            url : "/stockman/newstockdata/",
            type : "POST",
            data : {
                    globalClinicSelected : globalClinicSelected,
                    globalMedsSelected : globalMedsSelected,
                    globalStockAmt : globalStockAmt

                },

          success : function(data) {


            $( ".clinicDataRemove" ).empty();

            $( ".clinicDataRemove" ).remove();

            $( "#clinicNameX" ).remove();

            $("#clinicNameY").prepend( '<h3 id = "clinicNameX"' + 'class ="bg-warning text-align-center">'
                                 +  "Stock Updated For Clinic: " + globalClinicSelected
                                 +  "</h3>")

            for (var i = 0; i < data.length; i++) {


      $("#clinicData").prepend( "<tr class ='clinicDataRemove'>"
                           + "<th> " + data[i][0] + "</th>"
                           + "<th> " + data[i][1] + "</th>"
                           + "<th> " + data[i][2] + "</th>"

                            +"</tr>")
                  }
                              },

          error : function(xhr,errmsg,err) {


          alert("An internal Error Occured :" + errmsg + " Please Refresh the Page and Try again: If it still does not work then there is a really big issue!!!")

                }
});
};


// NOTE the CSRF Handling code below was taken from Django Docs directly.

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


});
