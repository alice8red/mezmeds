// This Script handles Downdown List And Post Requests Related to Them

$(function() {

load_Orders()

globalClinicSelected = "";
globalMedsSelected =  "";
globalStockSKU     = "";
//GET CLINICS
function load_Orders() {


  $.ajax({
      url : "/stockman/diliverylist",
      type : "GET",

      success: function(data){
        console.log(data);



        for (var i = 0; i < data.length; i++) {

        $("#clinicOrderData").append( "<tr class ='intial'>"
                       + "<th> " + data[i][4] + "</th>"
                       + "<th> " + data[i][0] + "</th>"
                       + "<th class='text-align-center'> " + data[i][1] + "</th>"
                       + "<th class='text-align-center'> " + data[i][2] + "</th>"
                       + "<th class='text-align-center'> " + data[i][3] + "</th>"
                       +"</tr>")
              }

       $('#orderMedsZ').on('submit', function(event){

        event.preventDefault();

       globalStockSKU = $('#stockvalue').val();
       $('#orderMedsZ')[0].reset();


       post_dilivery_data(globalStockSKU)

      });

        },

      error : function(xhr,errmsg,err) {


      alert("An internal Error Occured :" + errmsg + " Please Refresh the Page and Try again: If it still does not work then there is a really big issue!!!")

          }
});
};



// AJAX for posting
function post_dilivery_data(globalStockSKU) {

        $.ajax({
            url : "/stockman/diliveryrecieved/",
            type : "POST",
            data : {
                  globalStockSKU : globalStockSKU,

                },

          success : function(SKU) {


            $("#successOrderMessage").prepend( "<br>"+'<h4 id = "clinicNameX"' + 'class ="bg-warning text-align-center">'
                                   +  "****STOCK UPDATED*** Order Ref: "+ SKU +"<br>" +
                                   "</h4>")

                                  },

          error : function(xhr,errmsg,err) {


          alert("An internal Error Occured :" + errmsg + " Please Refresh the Page and Try again: If it still does not work then there is a really big issue!!!")

                }
});
};


// NOTE the CSRF Handling code below was taken from Django Docs directly.

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


});
