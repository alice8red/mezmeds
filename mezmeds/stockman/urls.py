from django.conf.urls import url, include

from . import views
urlpatterns = [

    #SITE

    url(r'^stockman/$', views.index, name='stockman'),
    url(r'^clinicorders/$', views.indexOrder, name='clinicorders'),
    url(r'^dilivery/$', views.indexDeliveries, name='dilivery'),

    # AJAX

    url(r'^clinicdata/$', views.get_clinic_data, name='clinicdata'),
    url(r'^newstockdata/$', views.post_stock_level, name='newstockdata'),
    url(r'^clinicrestocklist/$', views.get_restock_stock_level, name='clinicrestocklist'),
    url(r'^clinicorderspost/$', views.post_order_stock, name='clinicorderspost'),
    url(r'^diliverylist/$', views.dilivery_list, name='diliverylist'),
    url(r'^diliveryrecieved/$', views.dilivery_recieved, name='diliveryrecieved'),


]
