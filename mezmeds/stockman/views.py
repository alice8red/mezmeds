import json

from django.shortcuts import render
from django.http import HttpResponse

from sqldirect.sqldirect import *

#!!! Should move "orders functions to a new app to be more modular ---> Refactor in Future

def index(request):



    return render(request, 'stockman/index.html')

# GETS CURRENT CLINIC DATA
def get_clinic_data(request):

    if request.method == 'POST':

        clinicSelected = request.POST.get('globalClinicSelected')

        clinicObj = ClinicIndividualStock(clinicSelected)

        clinic = clinicObj.getClinicData()


        data = json.dumps(clinic)


        return HttpResponse(
                data,
                content_type="application/json"
            )


    else:

            return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json"
        )
#UPDATES CURRENT STOCK LEVELS
def post_stock_level(request):

    if request.method == 'POST':

        clinicSelected = request.POST.get('globalClinicSelected')
        medsSelected = request.POST.get('globalMedsSelected')
        medsOrder = int(request.POST.get('globalStockAmt'))

        clinicObj = ClinicIndividualStock(clinicSelected)

        clinic = clinicObj.getClinicData()

        print(clinic)

        updateObj = StockUpdated(clinicSelected,medsSelected,medsOrder)

        updateObj.postStockCurrent()

        clinicObj = ClinicIndividualStock(clinicSelected)

        clinic = clinicObj.getClinicData()

        data = json.dumps(clinic)

        return HttpResponse(
                data,
                content_type="application/json"
            )


    else:

            return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json"
        )



# WARNS USER ABOUT STOCK IN CLINIC WHICH MEDS < 5 UNIT.*
def get_restock_stock_level(request):

    if request.method == 'GET':

        clinicRestockObj = StockUpdated()

        clinicRestock = clinicRestockObj.getAllRestock()

        data = json.dumps(clinicRestock)


        return HttpResponse(
                data,
                content_type="application/json"
            )


    else:

            return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json"
        )

#############ORDERS################# // Should be in different app -> Future iteration

def indexOrder(request):


     return render(request, 'stockman/order.html')



def post_order_stock(request):

    if request.method == 'POST':


        clinicSelected = request.POST.get('globalClinicSelected')
        medsSelected = request.POST.get('globalMedsSelected')
        medsOrder = int(request.POST.get('globalStockAmt'))

#!DB! truthValue dependent on db ;in sqlite at current db: boolan is 0/1
        truthValue = 0


        clinicRestockObj = StockUpdated(clinicSelected,medsSelected,medsOrder)
        print(clinicRestockObj)

        clinicRestockObj.orderStock(medsOrder,truthValue)


        return HttpResponse(

            )


    else:

            return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json"
        )



#Deliviers

def indexDeliveries(request):



    return render(request, 'stockman/dilivery.html')


def dilivery_list(request):

    if request.method == 'GET':

        clinicOrderObj = StockUpdated()

        clinicOrders = clinicOrderObj.getAllDiliviers()

        data = json.dumps(clinicOrders)


        return HttpResponse(
                data,
                content_type="application/json"
            )


    else:

            return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json"
        )

def dilivery_recieved(request):

    if request.method == 'POST':

        globalStockSKU = request.POST.get('globalStockSKU')

        diliviersRecieved = StockUpdated()

        diliviersRecieved.postDiliviersRecieved(globalStockSKU)



        data = json.dumps(globalStockSKU)


        return HttpResponse(
                data,
                content_type="application/json"
            )


    else:

            return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json"
        )
