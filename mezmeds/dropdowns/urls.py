from django.conf.urls import url, include
from . import views

urlpatterns = [

    url(r'^cliniclist/$', views.get_clinic_and_medicine_lists, name='cliniclist'),

    # api
    # url(r'^api/v1/todos/$',views.todo_all ,name= 'todo_all'),

]
