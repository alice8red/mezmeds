import json

from django.shortcuts import render
from django.http import HttpResponse

from sqldirect.sqldirect import *

# Create your views here.

def get_clinic_and_medicine_lists(request):

    if request.method == 'GET':

        tmp =[]

        clinicsAllObj = ClinicsAll()

        clinics = clinicsAllObj.getAll()

        tmp.append(clinics)

        MedsAllObj = MedicationAll()

        meds = MedsAllObj.getAll()
        tmp.append(meds)

        data = json.dumps(tmp)

        return HttpResponse(
                data,
                content_type="application/json"
            )


    else:

            return HttpResponse(
            json.dumps({"THIS IS AN ERROR THAT WAS NOT HANDLED"}),
            content_type="application/json"
        )
